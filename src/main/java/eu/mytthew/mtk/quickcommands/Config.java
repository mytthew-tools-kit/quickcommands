package eu.mytthew.mtk.quickcommands;

import lombok.Data;

@Data
public class Config {
	RandomCollection<String> textCollection = new RandomCollection<>();
}
