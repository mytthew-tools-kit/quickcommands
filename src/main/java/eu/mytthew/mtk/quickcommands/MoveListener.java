package eu.mytthew.mtk.quickcommands;

import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerMoveEvent;

public class MoveListener implements Listener {
	@EventHandler
	public void playerMovingListening(PlayerMoveEvent playerMoveEvent) {
		Location currentPos = playerMoveEvent.getTo().clone();

		Location currentEastPos = playerMoveEvent.getTo().clone();
		Location currentWestPos = playerMoveEvent.getTo().clone();
		Location currentSouthPos = playerMoveEvent.getTo().clone();
		Location currentNorthPos = playerMoveEvent.getTo().clone();

		Location firstCorner = playerMoveEvent.getTo().clone();
		Location secondCorner = playerMoveEvent.getTo().clone();
		Location thirdCorner = playerMoveEvent.getTo().clone();
		Location fourthCorner = playerMoveEvent.getTo().clone();

		Location eastPos = currentEastPos.add(1, -1, 0);
		Location westPos = currentWestPos.add(-1, -1, 0);
		Location soutPos = currentSouthPos.add(0, -1, 1);
		Location northPos = currentNorthPos.add(0, -1, -1);

		Location firstCornerPos = firstCorner.add(1, -1, 1);
		Location secondCornerPos = secondCorner.add(-1, -1, -1);
		Location thirdCornerPos = thirdCorner.add(-1, -1, 1);
		Location fourthCornerPos = fourthCorner.add(1, -1, -1);

		eastPos.getBlock().setType(Material.GLASS);
		westPos.getBlock().setType(Material.GLASS);
		soutPos.getBlock().setType(Material.GLASS);
		northPos.getBlock().setType(Material.GLASS);
		firstCornerPos.getBlock().setType(Material.GLASS);
		secondCornerPos.getBlock().setType(Material.GLASS);
		thirdCornerPos.getBlock().setType(Material.GLASS);
		fourthCornerPos.getBlock().setType(Material.GLASS);
	}
}
