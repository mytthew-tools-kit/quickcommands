package eu.mytthew.mtk.quickcommands;

import org.bukkit.Material;
import org.bukkit.command.CommandExecutor;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.ShapedRecipe;
import org.bukkit.inventory.meta.ItemMeta;
import org.bukkit.plugin.java.JavaPlugin;

import java.util.HashMap;
import java.util.Map;
import java.util.Objects;

public class Main extends JavaPlugin {
	private final Config config = new Config();

	public String getRandomText() {
		return config.getTextCollection().next();
	}

	public void diamondChest() {
		ItemStack diamondChest = new ItemStack(Material.CHEST);
		if (diamondChest.getItemMeta() != null) {
			ItemMeta newItemMeta = diamondChest.getItemMeta();
			newItemMeta.setDisplayName("Diamond chest");
			diamondChest.setItemMeta(newItemMeta);
			ShapedRecipe diamondChestRecipe = new ShapedRecipe(diamondChest)
					.shape("LLL", "OSO", "LLL")
					.setIngredient('L', Material.SPRUCE_LOG)
					.setIngredient('O', Material.OBSIDIAN)
					.setIngredient('S', Material.SLIME_BALL);
			getServer().addRecipe(diamondChestRecipe);
		}
	}

	private void loadConfig() {
		Objects.requireNonNull(getConfig()
				.getList("texts"))
				.stream()
				.filter(text -> text instanceof Map)
				.map(text -> getConfig().createSection("x", (Map<?, ?>) text))
				.forEach(text -> config.getTextCollection().add(text.getDouble("value"), text.getString("text")));
	}

	@Override
	public void onEnable() {
		Map<String, CommandExecutor> commands = new HashMap<>();
		commands.put("action", new ActionCommand(this));
		commands.put("clearinventory", new ClearInventoryCommand(this));
		commands.forEach((key, value) -> Objects.requireNonNull(getCommand(key)).setExecutor(value));
		saveDefaultConfig();
		loadConfig();
		getServer().getPluginManager().registerEvents(new ThrowListener(this), this);
		diamondChest();
	}
}
