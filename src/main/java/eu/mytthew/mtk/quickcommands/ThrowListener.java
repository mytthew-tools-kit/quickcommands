package eu.mytthew.mtk.quickcommands;

import lombok.AllArgsConstructor;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.World;
import org.bukkit.block.Block;
import org.bukkit.block.Chest;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.block.Action;
import org.bukkit.event.inventory.InventoryCloseEvent;
import org.bukkit.event.inventory.InventoryType;
import org.bukkit.event.player.PlayerEggThrowEvent;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;

import java.util.Objects;
import java.util.Optional;
import java.util.Random;

@AllArgsConstructor
public class ThrowListener implements Listener {
	private final Main main;
	private final Random random = new Random();

	@EventHandler
	public void throwingEggListener(PlayerEggThrowEvent playerEggThrowEvent) {
		playerEggThrowEvent.getPlayer().sendMessage(main.getRandomText());
	}

	@EventHandler
	public void diamondChestOpenEvent(PlayerInteractEvent playerInteractEvent) {
		Optional.ofNullable(playerInteractEvent)
				.map(PlayerInteractEvent::getClickedBlock)
				.map(Block::getState)
				.filter(blockState -> blockState instanceof Chest)
				.map(blockState -> (Chest) blockState)
				.filter(chest -> Objects.equals(chest.getCustomName(), "Diamond chest"))
				.filter(chest -> playerInteractEvent.getAction() == Action.RIGHT_CLICK_BLOCK)
				.filter(chest -> playerInteractEvent.getClickedBlock() != null)
				.map(Chest::getBlockInventory)
				.ifPresent(chest -> {
					chest.clear();
					chest.addItem(new ItemStack(Material.DIAMOND, random.nextInt(1700)));
				});
	}

	@EventHandler
	public void explodeChest(InventoryCloseEvent inventoryCloseEvent) {
		World w = inventoryCloseEvent.getPlayer().getWorld();
		if (inventoryCloseEvent.getInventory().getType().equals(InventoryType.CHEST)) {
			Optional.of(inventoryCloseEvent)
					.map(InventoryCloseEvent::getInventory)
					.map(Inventory::getLocation)
					.map(Location::getBlock)
					.map(Block::getState)
					.filter(blockState -> blockState instanceof Chest)
					.map(blockState -> (Chest) blockState)
					.filter(chest -> Objects.equals(chest.getCustomName(), "Diamond chest"))
					.map(Chest::getLocation)
					.ifPresent(location -> w.createExplosion(location, 3, false));
		}
	}
}
